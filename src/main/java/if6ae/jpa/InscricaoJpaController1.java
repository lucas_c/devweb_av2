/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if6ae.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import if6ae.entity.Categoria;
import if6ae.entity.Inscricao;
import if6ae.entity.InscricaoMinicurso;
import if6ae.jpa.exceptions.IllegalOrphanException;
import if6ae.jpa.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author lucas
 */
public class InscricaoJpaController1 implements Serializable {

    public InscricaoJpaController1(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Inscricao inscricao) {
        if (inscricao.getInscricaoMinicursoCollection() == null) {
            inscricao.setInscricaoMinicursoCollection(new ArrayList<InscricaoMinicurso>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Categoria categoria = inscricao.getCategoria();
            if (categoria != null) {
                categoria = em.getReference(categoria.getClass(), categoria.getCodigo());
                inscricao.setCategoria(categoria);
            }
            Collection<InscricaoMinicurso> attachedInscricaoMinicursoCollection = new ArrayList<InscricaoMinicurso>();
            for (InscricaoMinicurso inscricaoMinicursoCollectionInscricaoMinicursoToAttach : inscricao.getInscricaoMinicursoCollection()) {
                inscricaoMinicursoCollectionInscricaoMinicursoToAttach = em.getReference(inscricaoMinicursoCollectionInscricaoMinicursoToAttach.getClass(), inscricaoMinicursoCollectionInscricaoMinicursoToAttach.getInscricaoMinicursoPK());
                attachedInscricaoMinicursoCollection.add(inscricaoMinicursoCollectionInscricaoMinicursoToAttach);
            }
            inscricao.setInscricaoMinicursoCollection(attachedInscricaoMinicursoCollection);
            em.persist(inscricao);
            if (categoria != null) {
                categoria.getInscricaoCollection().add(inscricao);
                categoria = em.merge(categoria);
            }
            for (InscricaoMinicurso inscricaoMinicursoCollectionInscricaoMinicurso : inscricao.getInscricaoMinicursoCollection()) {
                Inscricao oldInscricaoOfInscricaoMinicursoCollectionInscricaoMinicurso = inscricaoMinicursoCollectionInscricaoMinicurso.getInscricao();
                inscricaoMinicursoCollectionInscricaoMinicurso.setInscricao(inscricao);
                inscricaoMinicursoCollectionInscricaoMinicurso = em.merge(inscricaoMinicursoCollectionInscricaoMinicurso);
                if (oldInscricaoOfInscricaoMinicursoCollectionInscricaoMinicurso != null) {
                    oldInscricaoOfInscricaoMinicursoCollectionInscricaoMinicurso.getInscricaoMinicursoCollection().remove(inscricaoMinicursoCollectionInscricaoMinicurso);
                    oldInscricaoOfInscricaoMinicursoCollectionInscricaoMinicurso = em.merge(oldInscricaoOfInscricaoMinicursoCollectionInscricaoMinicurso);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Inscricao inscricao) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Inscricao persistentInscricao = em.find(Inscricao.class, inscricao.getNumero());
            Categoria categoriaOld = persistentInscricao.getCategoria();
            Categoria categoriaNew = inscricao.getCategoria();
            Collection<InscricaoMinicurso> inscricaoMinicursoCollectionOld = persistentInscricao.getInscricaoMinicursoCollection();
            Collection<InscricaoMinicurso> inscricaoMinicursoCollectionNew = inscricao.getInscricaoMinicursoCollection();
            List<String> illegalOrphanMessages = null;
            for (InscricaoMinicurso inscricaoMinicursoCollectionOldInscricaoMinicurso : inscricaoMinicursoCollectionOld) {
                if (!inscricaoMinicursoCollectionNew.contains(inscricaoMinicursoCollectionOldInscricaoMinicurso)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain InscricaoMinicurso " + inscricaoMinicursoCollectionOldInscricaoMinicurso + " since its inscricao field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (categoriaNew != null) {
                categoriaNew = em.getReference(categoriaNew.getClass(), categoriaNew.getCodigo());
                inscricao.setCategoria(categoriaNew);
            }
            Collection<InscricaoMinicurso> attachedInscricaoMinicursoCollectionNew = new ArrayList<InscricaoMinicurso>();
            for (InscricaoMinicurso inscricaoMinicursoCollectionNewInscricaoMinicursoToAttach : inscricaoMinicursoCollectionNew) {
                inscricaoMinicursoCollectionNewInscricaoMinicursoToAttach = em.getReference(inscricaoMinicursoCollectionNewInscricaoMinicursoToAttach.getClass(), inscricaoMinicursoCollectionNewInscricaoMinicursoToAttach.getInscricaoMinicursoPK());
                attachedInscricaoMinicursoCollectionNew.add(inscricaoMinicursoCollectionNewInscricaoMinicursoToAttach);
            }
            inscricaoMinicursoCollectionNew = attachedInscricaoMinicursoCollectionNew;
            inscricao.setInscricaoMinicursoCollection(inscricaoMinicursoCollectionNew);
            inscricao = em.merge(inscricao);
            if (categoriaOld != null && !categoriaOld.equals(categoriaNew)) {
                categoriaOld.getInscricaoCollection().remove(inscricao);
                categoriaOld = em.merge(categoriaOld);
            }
            if (categoriaNew != null && !categoriaNew.equals(categoriaOld)) {
                categoriaNew.getInscricaoCollection().add(inscricao);
                categoriaNew = em.merge(categoriaNew);
            }
            for (InscricaoMinicurso inscricaoMinicursoCollectionNewInscricaoMinicurso : inscricaoMinicursoCollectionNew) {
                if (!inscricaoMinicursoCollectionOld.contains(inscricaoMinicursoCollectionNewInscricaoMinicurso)) {
                    Inscricao oldInscricaoOfInscricaoMinicursoCollectionNewInscricaoMinicurso = inscricaoMinicursoCollectionNewInscricaoMinicurso.getInscricao();
                    inscricaoMinicursoCollectionNewInscricaoMinicurso.setInscricao(inscricao);
                    inscricaoMinicursoCollectionNewInscricaoMinicurso = em.merge(inscricaoMinicursoCollectionNewInscricaoMinicurso);
                    if (oldInscricaoOfInscricaoMinicursoCollectionNewInscricaoMinicurso != null && !oldInscricaoOfInscricaoMinicursoCollectionNewInscricaoMinicurso.equals(inscricao)) {
                        oldInscricaoOfInscricaoMinicursoCollectionNewInscricaoMinicurso.getInscricaoMinicursoCollection().remove(inscricaoMinicursoCollectionNewInscricaoMinicurso);
                        oldInscricaoOfInscricaoMinicursoCollectionNewInscricaoMinicurso = em.merge(oldInscricaoOfInscricaoMinicursoCollectionNewInscricaoMinicurso);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = inscricao.getNumero();
                if (findInscricao(id) == null) {
                    throw new NonexistentEntityException("The inscricao with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Inscricao inscricao;
            try {
                inscricao = em.getReference(Inscricao.class, id);
                inscricao.getNumero();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The inscricao with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<InscricaoMinicurso> inscricaoMinicursoCollectionOrphanCheck = inscricao.getInscricaoMinicursoCollection();
            for (InscricaoMinicurso inscricaoMinicursoCollectionOrphanCheckInscricaoMinicurso : inscricaoMinicursoCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Inscricao (" + inscricao + ") cannot be destroyed since the InscricaoMinicurso " + inscricaoMinicursoCollectionOrphanCheckInscricaoMinicurso + " in its inscricaoMinicursoCollection field has a non-nullable inscricao field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Categoria categoria = inscricao.getCategoria();
            if (categoria != null) {
                categoria.getInscricaoCollection().remove(inscricao);
                categoria = em.merge(categoria);
            }
            em.remove(inscricao);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Inscricao> findInscricaoEntities() {
        return findInscricaoEntities(true, -1, -1);
    }

    public List<Inscricao> findInscricaoEntities(int maxResults, int firstResult) {
        return findInscricaoEntities(false, maxResults, firstResult);
    }

    private List<Inscricao> findInscricaoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Inscricao.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Inscricao findInscricao(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Inscricao.class, id);
        } finally {
            em.close();
        }
    }

    public int getInscricaoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Inscricao> rt = cq.from(Inscricao.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
