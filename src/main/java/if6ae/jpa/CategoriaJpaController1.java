/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if6ae.jpa;

import if6ae.entity.Categoria;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import if6ae.entity.Inscricao;
import if6ae.jpa.exceptions.IllegalOrphanException;
import if6ae.jpa.exceptions.NonexistentEntityException;
import if6ae.jpa.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author lucas
 */
public class CategoriaJpaController1 implements Serializable {

    public CategoriaJpaController1(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Categoria categoria) throws PreexistingEntityException, Exception {
        if (categoria.getInscricaoCollection() == null) {
            categoria.setInscricaoCollection(new ArrayList<Inscricao>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Inscricao> attachedInscricaoCollection = new ArrayList<Inscricao>();
            for (Inscricao inscricaoCollectionInscricaoToAttach : categoria.getInscricaoCollection()) {
                inscricaoCollectionInscricaoToAttach = em.getReference(inscricaoCollectionInscricaoToAttach.getClass(), inscricaoCollectionInscricaoToAttach.getNumero());
                attachedInscricaoCollection.add(inscricaoCollectionInscricaoToAttach);
            }
            categoria.setInscricaoCollection(attachedInscricaoCollection);
            em.persist(categoria);
            for (Inscricao inscricaoCollectionInscricao : categoria.getInscricaoCollection()) {
                Categoria oldCategoriaOfInscricaoCollectionInscricao = inscricaoCollectionInscricao.getCategoria();
                inscricaoCollectionInscricao.setCategoria(categoria);
                inscricaoCollectionInscricao = em.merge(inscricaoCollectionInscricao);
                if (oldCategoriaOfInscricaoCollectionInscricao != null) {
                    oldCategoriaOfInscricaoCollectionInscricao.getInscricaoCollection().remove(inscricaoCollectionInscricao);
                    oldCategoriaOfInscricaoCollectionInscricao = em.merge(oldCategoriaOfInscricaoCollectionInscricao);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCategoria(categoria.getCodigo()) != null) {
                throw new PreexistingEntityException("Categoria " + categoria + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Categoria categoria) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Categoria persistentCategoria = em.find(Categoria.class, categoria.getCodigo());
            Collection<Inscricao> inscricaoCollectionOld = persistentCategoria.getInscricaoCollection();
            Collection<Inscricao> inscricaoCollectionNew = categoria.getInscricaoCollection();
            List<String> illegalOrphanMessages = null;
            for (Inscricao inscricaoCollectionOldInscricao : inscricaoCollectionOld) {
                if (!inscricaoCollectionNew.contains(inscricaoCollectionOldInscricao)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Inscricao " + inscricaoCollectionOldInscricao + " since its categoria field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Inscricao> attachedInscricaoCollectionNew = new ArrayList<Inscricao>();
            for (Inscricao inscricaoCollectionNewInscricaoToAttach : inscricaoCollectionNew) {
                inscricaoCollectionNewInscricaoToAttach = em.getReference(inscricaoCollectionNewInscricaoToAttach.getClass(), inscricaoCollectionNewInscricaoToAttach.getNumero());
                attachedInscricaoCollectionNew.add(inscricaoCollectionNewInscricaoToAttach);
            }
            inscricaoCollectionNew = attachedInscricaoCollectionNew;
            categoria.setInscricaoCollection(inscricaoCollectionNew);
            categoria = em.merge(categoria);
            for (Inscricao inscricaoCollectionNewInscricao : inscricaoCollectionNew) {
                if (!inscricaoCollectionOld.contains(inscricaoCollectionNewInscricao)) {
                    Categoria oldCategoriaOfInscricaoCollectionNewInscricao = inscricaoCollectionNewInscricao.getCategoria();
                    inscricaoCollectionNewInscricao.setCategoria(categoria);
                    inscricaoCollectionNewInscricao = em.merge(inscricaoCollectionNewInscricao);
                    if (oldCategoriaOfInscricaoCollectionNewInscricao != null && !oldCategoriaOfInscricaoCollectionNewInscricao.equals(categoria)) {
                        oldCategoriaOfInscricaoCollectionNewInscricao.getInscricaoCollection().remove(inscricaoCollectionNewInscricao);
                        oldCategoriaOfInscricaoCollectionNewInscricao = em.merge(oldCategoriaOfInscricaoCollectionNewInscricao);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = categoria.getCodigo();
                if (findCategoria(id) == null) {
                    throw new NonexistentEntityException("The categoria with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Categoria categoria;
            try {
                categoria = em.getReference(Categoria.class, id);
                categoria.getCodigo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The categoria with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Inscricao> inscricaoCollectionOrphanCheck = categoria.getInscricaoCollection();
            for (Inscricao inscricaoCollectionOrphanCheckInscricao : inscricaoCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Categoria (" + categoria + ") cannot be destroyed since the Inscricao " + inscricaoCollectionOrphanCheckInscricao + " in its inscricaoCollection field has a non-nullable categoria field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(categoria);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Categoria> findCategoriaEntities() {
        return findCategoriaEntities(true, -1, -1);
    }

    public List<Categoria> findCategoriaEntities(int maxResults, int firstResult) {
        return findCategoriaEntities(false, maxResults, firstResult);
    }

    private List<Categoria> findCategoriaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Categoria.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Categoria findCategoria(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Categoria.class, id);
        } finally {
            em.close();
        }
    }

    public int getCategoriaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Categoria> rt = cq.from(Categoria.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
