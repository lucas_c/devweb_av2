/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if6ae.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import if6ae.entity.InscricaoMinicurso;
import if6ae.entity.Minicurso;
import if6ae.jpa.exceptions.IllegalOrphanException;
import if6ae.jpa.exceptions.NonexistentEntityException;
import if6ae.jpa.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author lucas
 */
public class MinicursoJpaController implements Serializable {

    public MinicursoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Minicurso minicurso) throws PreexistingEntityException, Exception {
        if (minicurso.getInscricaoMinicursoCollection() == null) {
            minicurso.setInscricaoMinicursoCollection(new ArrayList<InscricaoMinicurso>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<InscricaoMinicurso> attachedInscricaoMinicursoCollection = new ArrayList<InscricaoMinicurso>();
            for (InscricaoMinicurso inscricaoMinicursoCollectionInscricaoMinicursoToAttach : minicurso.getInscricaoMinicursoCollection()) {
                inscricaoMinicursoCollectionInscricaoMinicursoToAttach = em.getReference(inscricaoMinicursoCollectionInscricaoMinicursoToAttach.getClass(), inscricaoMinicursoCollectionInscricaoMinicursoToAttach.getInscricaoMinicursoPK());
                attachedInscricaoMinicursoCollection.add(inscricaoMinicursoCollectionInscricaoMinicursoToAttach);
            }
            minicurso.setInscricaoMinicursoCollection(attachedInscricaoMinicursoCollection);
            em.persist(minicurso);
            for (InscricaoMinicurso inscricaoMinicursoCollectionInscricaoMinicurso : minicurso.getInscricaoMinicursoCollection()) {
                Minicurso oldMinicurso1OfInscricaoMinicursoCollectionInscricaoMinicurso = inscricaoMinicursoCollectionInscricaoMinicurso.getMinicurso1();
                inscricaoMinicursoCollectionInscricaoMinicurso.setMinicurso1(minicurso);
                inscricaoMinicursoCollectionInscricaoMinicurso = em.merge(inscricaoMinicursoCollectionInscricaoMinicurso);
                if (oldMinicurso1OfInscricaoMinicursoCollectionInscricaoMinicurso != null) {
                    oldMinicurso1OfInscricaoMinicursoCollectionInscricaoMinicurso.getInscricaoMinicursoCollection().remove(inscricaoMinicursoCollectionInscricaoMinicurso);
                    oldMinicurso1OfInscricaoMinicursoCollectionInscricaoMinicurso = em.merge(oldMinicurso1OfInscricaoMinicursoCollectionInscricaoMinicurso);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMinicurso(minicurso.getCodigo()) != null) {
                throw new PreexistingEntityException("Minicurso " + minicurso + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Minicurso minicurso) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Minicurso persistentMinicurso = em.find(Minicurso.class, minicurso.getCodigo());
            Collection<InscricaoMinicurso> inscricaoMinicursoCollectionOld = persistentMinicurso.getInscricaoMinicursoCollection();
            Collection<InscricaoMinicurso> inscricaoMinicursoCollectionNew = minicurso.getInscricaoMinicursoCollection();
            List<String> illegalOrphanMessages = null;
            for (InscricaoMinicurso inscricaoMinicursoCollectionOldInscricaoMinicurso : inscricaoMinicursoCollectionOld) {
                if (!inscricaoMinicursoCollectionNew.contains(inscricaoMinicursoCollectionOldInscricaoMinicurso)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain InscricaoMinicurso " + inscricaoMinicursoCollectionOldInscricaoMinicurso + " since its minicurso1 field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<InscricaoMinicurso> attachedInscricaoMinicursoCollectionNew = new ArrayList<InscricaoMinicurso>();
            for (InscricaoMinicurso inscricaoMinicursoCollectionNewInscricaoMinicursoToAttach : inscricaoMinicursoCollectionNew) {
                inscricaoMinicursoCollectionNewInscricaoMinicursoToAttach = em.getReference(inscricaoMinicursoCollectionNewInscricaoMinicursoToAttach.getClass(), inscricaoMinicursoCollectionNewInscricaoMinicursoToAttach.getInscricaoMinicursoPK());
                attachedInscricaoMinicursoCollectionNew.add(inscricaoMinicursoCollectionNewInscricaoMinicursoToAttach);
            }
            inscricaoMinicursoCollectionNew = attachedInscricaoMinicursoCollectionNew;
            minicurso.setInscricaoMinicursoCollection(inscricaoMinicursoCollectionNew);
            minicurso = em.merge(minicurso);
            for (InscricaoMinicurso inscricaoMinicursoCollectionNewInscricaoMinicurso : inscricaoMinicursoCollectionNew) {
                if (!inscricaoMinicursoCollectionOld.contains(inscricaoMinicursoCollectionNewInscricaoMinicurso)) {
                    Minicurso oldMinicurso1OfInscricaoMinicursoCollectionNewInscricaoMinicurso = inscricaoMinicursoCollectionNewInscricaoMinicurso.getMinicurso1();
                    inscricaoMinicursoCollectionNewInscricaoMinicurso.setMinicurso1(minicurso);
                    inscricaoMinicursoCollectionNewInscricaoMinicurso = em.merge(inscricaoMinicursoCollectionNewInscricaoMinicurso);
                    if (oldMinicurso1OfInscricaoMinicursoCollectionNewInscricaoMinicurso != null && !oldMinicurso1OfInscricaoMinicursoCollectionNewInscricaoMinicurso.equals(minicurso)) {
                        oldMinicurso1OfInscricaoMinicursoCollectionNewInscricaoMinicurso.getInscricaoMinicursoCollection().remove(inscricaoMinicursoCollectionNewInscricaoMinicurso);
                        oldMinicurso1OfInscricaoMinicursoCollectionNewInscricaoMinicurso = em.merge(oldMinicurso1OfInscricaoMinicursoCollectionNewInscricaoMinicurso);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = minicurso.getCodigo();
                if (findMinicurso(id) == null) {
                    throw new NonexistentEntityException("The minicurso with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Minicurso minicurso;
            try {
                minicurso = em.getReference(Minicurso.class, id);
                minicurso.getCodigo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The minicurso with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<InscricaoMinicurso> inscricaoMinicursoCollectionOrphanCheck = minicurso.getInscricaoMinicursoCollection();
            for (InscricaoMinicurso inscricaoMinicursoCollectionOrphanCheckInscricaoMinicurso : inscricaoMinicursoCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Minicurso (" + minicurso + ") cannot be destroyed since the InscricaoMinicurso " + inscricaoMinicursoCollectionOrphanCheckInscricaoMinicurso + " in its inscricaoMinicursoCollection field has a non-nullable minicurso1 field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(minicurso);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Minicurso> findMinicursoEntities() {
        return findMinicursoEntities(true, -1, -1);
    }

    public List<Minicurso> findMinicursoEntities(int maxResults, int firstResult) {
        return findMinicursoEntities(false, maxResults, firstResult);
    }

    private List<Minicurso> findMinicursoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Minicurso.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Minicurso findMinicurso(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Minicurso.class, id);
        } finally {
            em.close();
        }
    }

    public int getMinicursoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Minicurso> rt = cq.from(Minicurso.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
