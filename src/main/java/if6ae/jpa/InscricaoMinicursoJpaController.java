/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if6ae.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import if6ae.entity.Inscricao;
import if6ae.entity.InscricaoMinicurso;
import if6ae.entity.InscricaoMinicursoPK;
import if6ae.entity.Minicurso;
import if6ae.jpa.exceptions.NonexistentEntityException;
import if6ae.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author lucas
 */
public class InscricaoMinicursoJpaController implements Serializable {

    public InscricaoMinicursoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(InscricaoMinicurso inscricaoMinicurso) throws PreexistingEntityException, Exception {
        if (inscricaoMinicurso.getInscricaoMinicursoPK() == null) {
            inscricaoMinicurso.setInscricaoMinicursoPK(new InscricaoMinicursoPK());
        }
        inscricaoMinicurso.getInscricaoMinicursoPK().setMinicurso(inscricaoMinicurso.getMinicurso1().getCodigo());
        inscricaoMinicurso.getInscricaoMinicursoPK().setNumeroInscricao(inscricaoMinicurso.getInscricao().getNumero());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Inscricao inscricao = inscricaoMinicurso.getInscricao();
            if (inscricao != null) {
                inscricao = em.getReference(inscricao.getClass(), inscricao.getNumero());
                inscricaoMinicurso.setInscricao(inscricao);
            }
            Minicurso minicurso1 = inscricaoMinicurso.getMinicurso1();
            if (minicurso1 != null) {
                minicurso1 = em.getReference(minicurso1.getClass(), minicurso1.getCodigo());
                inscricaoMinicurso.setMinicurso1(minicurso1);
            }
            em.persist(inscricaoMinicurso);
            if (inscricao != null) {
                inscricao.getInscricaoMinicursoCollection().add(inscricaoMinicurso);
                inscricao = em.merge(inscricao);
            }
            if (minicurso1 != null) {
                minicurso1.getInscricaoMinicursoCollection().add(inscricaoMinicurso);
                minicurso1 = em.merge(minicurso1);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findInscricaoMinicurso(inscricaoMinicurso.getInscricaoMinicursoPK()) != null) {
                throw new PreexistingEntityException("InscricaoMinicurso " + inscricaoMinicurso + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(InscricaoMinicurso inscricaoMinicurso) throws NonexistentEntityException, Exception {
        inscricaoMinicurso.getInscricaoMinicursoPK().setMinicurso(inscricaoMinicurso.getMinicurso1().getCodigo());
        inscricaoMinicurso.getInscricaoMinicursoPK().setNumeroInscricao(inscricaoMinicurso.getInscricao().getNumero());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            InscricaoMinicurso persistentInscricaoMinicurso = em.find(InscricaoMinicurso.class, inscricaoMinicurso.getInscricaoMinicursoPK());
            Inscricao inscricaoOld = persistentInscricaoMinicurso.getInscricao();
            Inscricao inscricaoNew = inscricaoMinicurso.getInscricao();
            Minicurso minicurso1Old = persistentInscricaoMinicurso.getMinicurso1();
            Minicurso minicurso1New = inscricaoMinicurso.getMinicurso1();
            if (inscricaoNew != null) {
                inscricaoNew = em.getReference(inscricaoNew.getClass(), inscricaoNew.getNumero());
                inscricaoMinicurso.setInscricao(inscricaoNew);
            }
            if (minicurso1New != null) {
                minicurso1New = em.getReference(minicurso1New.getClass(), minicurso1New.getCodigo());
                inscricaoMinicurso.setMinicurso1(minicurso1New);
            }
            inscricaoMinicurso = em.merge(inscricaoMinicurso);
            if (inscricaoOld != null && !inscricaoOld.equals(inscricaoNew)) {
                inscricaoOld.getInscricaoMinicursoCollection().remove(inscricaoMinicurso);
                inscricaoOld = em.merge(inscricaoOld);
            }
            if (inscricaoNew != null && !inscricaoNew.equals(inscricaoOld)) {
                inscricaoNew.getInscricaoMinicursoCollection().add(inscricaoMinicurso);
                inscricaoNew = em.merge(inscricaoNew);
            }
            if (minicurso1Old != null && !minicurso1Old.equals(minicurso1New)) {
                minicurso1Old.getInscricaoMinicursoCollection().remove(inscricaoMinicurso);
                minicurso1Old = em.merge(minicurso1Old);
            }
            if (minicurso1New != null && !minicurso1New.equals(minicurso1Old)) {
                minicurso1New.getInscricaoMinicursoCollection().add(inscricaoMinicurso);
                minicurso1New = em.merge(minicurso1New);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                InscricaoMinicursoPK id = inscricaoMinicurso.getInscricaoMinicursoPK();
                if (findInscricaoMinicurso(id) == null) {
                    throw new NonexistentEntityException("The inscricaoMinicurso with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(InscricaoMinicursoPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            InscricaoMinicurso inscricaoMinicurso;
            try {
                inscricaoMinicurso = em.getReference(InscricaoMinicurso.class, id);
                inscricaoMinicurso.getInscricaoMinicursoPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The inscricaoMinicurso with id " + id + " no longer exists.", enfe);
            }
            Inscricao inscricao = inscricaoMinicurso.getInscricao();
            if (inscricao != null) {
                inscricao.getInscricaoMinicursoCollection().remove(inscricaoMinicurso);
                inscricao = em.merge(inscricao);
            }
            Minicurso minicurso1 = inscricaoMinicurso.getMinicurso1();
            if (minicurso1 != null) {
                minicurso1.getInscricaoMinicursoCollection().remove(inscricaoMinicurso);
                minicurso1 = em.merge(minicurso1);
            }
            em.remove(inscricaoMinicurso);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<InscricaoMinicurso> findInscricaoMinicursoEntities() {
        return findInscricaoMinicursoEntities(true, -1, -1);
    }

    public List<InscricaoMinicurso> findInscricaoMinicursoEntities(int maxResults, int firstResult) {
        return findInscricaoMinicursoEntities(false, maxResults, firstResult);
    }

    private List<InscricaoMinicurso> findInscricaoMinicursoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(InscricaoMinicurso.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public InscricaoMinicurso findInscricaoMinicurso(InscricaoMinicursoPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(InscricaoMinicurso.class, id);
        } finally {
            em.close();
        }
    }

    public int getInscricaoMinicursoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<InscricaoMinicurso> rt = cq.from(InscricaoMinicurso.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
